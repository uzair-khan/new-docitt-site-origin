import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-profile-3',
  templateUrl: './profile-3.component.html',
  styleUrls: ['./profile-3.component.scss']
})
export class Profile3Component implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }
goBack(){
  this.router.navigate(['launchpad/profile2']);
}
loadNext(){
  this.router.navigate(['launchpad/assets']);
}
}
