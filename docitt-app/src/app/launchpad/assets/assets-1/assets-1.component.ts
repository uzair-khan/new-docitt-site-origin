import { Component, OnInit } from '@angular/core';
import{Router} from '@angular/router';

@Component({
  selector: 'app-assets-1',
  templateUrl: './assets-1.component.html',
  styleUrls: ['./assets-1.component.scss']
})
export class Assets1Component implements OnInit {
isClick = false;
  constructor(private router:Router) { }

  ngOnInit() {
  }
  selectBank(el){
    this.isClick = !this.isClick;
  }
loadNext(){
  this.router.navigate(['launchpad/assets2']);
}
}
