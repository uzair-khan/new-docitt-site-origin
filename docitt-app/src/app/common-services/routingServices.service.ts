import { Router, Params } from "@angular/router";

export class RoutingServices {
    constructor(private route:Router){}
    navigatePageUrl(url:String){
        // JS ternary operator LHS = condition? if value:else value
        url !== "base"?this.route.navigate([url]):this.route.navigate(['']);
    }
    navigatePageRelative(url:String,relative:String){
        relative?this.route.navigate([url]):this.route.navigate(['']);
    }
    navigatePageParams(url:String,relative:String,params:Params){

    }
}